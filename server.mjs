import Koa from 'koa'
import Router from '@koa/router'
import serve from 'koa-static'
import koaBody from 'koa-body'
import { v4 as uuid } from 'uuid'

import { readFileSync } from 'fs'
import { copyFile, appendFile } from 'fs/promises'
import { join } from 'path'

const basePath = process.env.UPLOAD_PATH || '.'
const logFile = 'files.log'

const app = new Koa()
app.proxy = true
const router = new Router()
app.use(serve('static')).use(koaBody({ multipart: true, formidable: { maxFileSize: 5 * 1024 * 1024 } }))
app.use(router.routes()).use(router.allowedMethods())

const whitelist = Object.fromEntries(readFileSync('whitelist').toString().split('\n').filter(Boolean).map(x => x.split(',')))

router.post('/submit', async ctx => {
  const user = ctx.request.body?.stuid
  if (!user) return ctx.body = '\x1b[31;1m[FAIL ✗]\x1b[0m 请填写你的学号\n'
  if (!whitelist.hasOwnProperty(user)) return ctx.body = '\x1b[31;1m[FAIL ✗]\x1b[0m 学号不在名单中；请检查你填写的学号。\n'
  const path = ctx.request.files?.file?.filepath
  if (!path) return ctx.body = '\x1b[31;1m[FAIL ✗]\x1b[0m 文件未上传\n'
  const id = uuid()
  const filename = `${user}-${new Date().toISOString().replaceAll(':', '-')}-${id}.zip`
  const dest = join(basePath, filename)
  await copyFile(path, dest)
  await appendFile(logFile, `${ctx.ip},${user},${id}\n`)
  return ctx.body = `\x1b[32;1m[SUCC ✓]\x1b[0m Received OS2024 ${whitelist[user]} ${user}\n`
})

const port = parseInt(process.env.PORT || 8080)
app.listen(port)
